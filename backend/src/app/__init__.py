from fastapi import APIRouter

from app.v1.controller.auth import router as auth_router
from core.db.session import Base

router = APIRouter()

router.include_router(auth_router, prefix="/api/v1")

__all__ = ["Base"]
