from fastapi import FastAPI, Request
from starlette.responses import JSONResponse

from app import router
from core.exceptions import CustomException


def init_routers(app: FastAPI) -> None:
    """
    Initialize all routers.
    """
    app.include_router(router)


def init_listeners(app: FastAPI) -> None:
    @app.exception_handler(Exception)
    async def exception_handler(request: Request, exc: Exception):
        """
        Handler for all the custom exceptions raised within the app.
        """
        return JSONResponse(status_code=400, content={"message": str(exc)})

    @app.exception_handler(CustomException)
    async def custom_exception_handler(request: Request, exc: CustomException):
        """
        Handler for all the custom exceptions raised within the app.
        """
        return JSONResponse(status_code=400, content={"status": "FAILED", "message": exc.message})


def create_app():
    app = FastAPI(
        title="Vritam",
        description="",
        version="1.0.0",
        docs_url="/docs",
        redoc_url="/redoc",
        openapi_url="/openapi.json",
    )
    init_routers(app=app)
    init_listeners(app=app)
    return app


app = create_app()
