from fastapi import Depends
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.v1.models import UserModel
from core.db.session import db_session


class UserRepo:

    def __init__(self, db: AsyncSession = Depends(db_session)):
        self.session = db

    async def save(self, user: UserModel):
        self.session.add(user)
        return user

    async def get_user_by_email(self, email: str):
        query = await self.session.execute(select(UserModel).where(UserModel.email == email))
        return query.scalars().first()
