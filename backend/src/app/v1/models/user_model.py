import uuid
from datetime import datetime
from typing import Optional

from sqlalchemy import  ForeignKey
from sqlalchemy.orm import Mapped, mapped_column

from core.db.session import Base
from core.db.timestamp_mixin import TimestampMixin
from core.utils.hashing import Hash

class User(Base):
    __tablename__ = "user"

    id: Mapped[uuid.UUID] = mapped_column(default=uuid.uuid4, primary_key=True, index=True)
    user_first_name: Mapped[str] = mapped_column(index=True, nullable=False)
    user_last_name: Mapped[str] = mapped_column(index=True, nullable=False)
    mobile_no: Mapped[str] = mapped_column(index=True, nullable=False)
    email: Mapped[str]= mapped_column(unique=True, nullable=False)
    password:Mapped[str]=mapped_column(index=True, nullable=False)


class OTP (Base):
    __tablename__ = "otp"
    id: Mapped[uuid.UUID] = mapped_column(default=uuid.uuid4, primary_key=True, index=True)
    otp:Mapped[int] = mapped_column(index=True, nullable=False)
    email :Mapped[str]=mapped_column( ForeignKey('user.email'), nullable=False)

    @classmethod
    def create(cls,
               first_name: Optional[str],
               surname: Optional[str],
               email: str,
               password: str,

               ):


        password = Hash.make(string=password)
        return cls(first_name=first_name, surname=surname, email=email, password=password,created_at=datetime.now()
                   )
