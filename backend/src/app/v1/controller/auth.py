from fastapi import APIRouter, Depends

from app.v1.schema import LoginRequestSchema
from app.v1.schema.user import CreateUserRequest, CreateUserResponse, LoginResponse
from app.v1.service.user_command import UserCommandService

router = APIRouter()


@router.post('/signup', response_model=CreateUserResponse)
async def login(db: Session = Depends(get_db), email: str = Form(...), password: str = Form(...)):
	return await services.create_user(**request.dict())


@router.post('/login', response_model=LoginResponse)
async def login(request: LoginRequestSchema, services: UserCommandService = Depends(UserCommandService)):
	return await services.login(**request.dict())
