from fastapi import Depends, HTTPException
from pydantic import EmailStr

from app.v1.exception.user import UserAlreadyExist, UserNotFound
from app.v1.models import UserModel
from app.v1.repository.user_repo import UserRepo
from app.v1.schema.user import LoginResponse
from config import config
from core.utils.hashing import Hash
from core.utils.token_helper import access
from requests import Session
from starlette import schemas

from backend.src.app.v1 import models
from backend.src.core.db.session import SessionLocal
from backend.src.core.utils.hashing import pwd_context


def get_password_hash(password):
    res = pwd_context.hash(password)

    return res
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
class UserCommandService:

    def __init__(self, user_repo: UserRepo = Depends(UserRepo)):
        self.user_repo = user_repo

    def create_user(user: schemas.createUseRequest, db: Session = Depends(get_db)):
        existing_user = db.query(models.User).filter(models.User.email == user.email).first()
        if existing_user:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="email already registered")
        if user.password != user.confirm_pass:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Passwords do not match")

        db_user = models.User(user_first_name=user.user_first_name, user_last_name=user.user_last_name, email=user.email,
                             mobile_no=user.mobile_no, password=get_password_hash(user.password))
        db.add(db_user)
        db.commit()
        db.refresh(db_user)

        return db_user

    async def login(self, email: str, password: str):
        user_obj = await self.user_repo.get_user_by_email(email=email)
        if not user_obj:
            raise UserNotFound
        if not Hash.verify(hashed=user_obj.password, raw=password):
            raise ValueError("Invalid Credential")

        access_token = access.encode(payload={"id": str(user_obj.id)}, expire_period=config.ACCESS_TOKEN_EXP)

        return LoginResponse(access_token=access_token,token_type="bearer",)
