from app.v1 import constants
from core.exceptions import NotFoundException, AlreadyExistsException, BadRequestException


class UserNotFound(NotFoundException):
    message = constants.USER_NOT_FOUND


class UserAlreadyExist(AlreadyExistsException):
    message = constants.USER_ALREADY_EXIST


class InvalidCredentials(BadRequestException):
    message = constants.INVALID_CREDENTIAL
