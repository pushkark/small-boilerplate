from pydantic import BaseModel, EmailStr

from core.utils.api_model import APIModel


class LoginRequestSchema(BaseModel):
    email: str
    password: str


class LoginResponse(APIModel):
    access_token: str
    token_type: str


class CreateUserResponse(APIModel):
    pass


class CreateUserRequest(BaseModel):
    first_name: str
    surname: str
    email: EmailStr
    password: str
