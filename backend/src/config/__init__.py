import os
import pathlib
from typing import Union

from pydantic import BaseSettings, EmailStr

# Project Directories
ROOT = pathlib.Path(__file__).resolve().parent.parent


def get_env_file() -> Union[str, None]:
    """
    Get the env file path.
    """
    # look for .env file in parent hierarchy of the project
    # (useful for local development)
    current_depth = 0  # current depth in the hierarchy
    max_depth = 3  # maximum depth to look for .env file
    root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    while current_depth < max_depth:
        file = os.path.join(root, ".env")
        if os.path.exists(file):
            return str(file)
        root = os.path.dirname(root)
        current_depth += 1
    return None

class Settings(BaseSettings):
    APP_HOST: str = os.environ.get('APP_HOST')
    APP_PORT: int = os.environ.get('APP_PORT')
    FIRST_SUPERUSER: EmailStr = "admin@vritam.com"
    FIRST_SUPERUSER_PW: str = "vritamAdmin321!"
    # SQLALCHEMY_DATABASE_URI: str = "sqlite+aiosqlite:///vritam.db"
    SQLALCHEMY_DATABASE_URI: str = "postgresql+asyncpg://postgres:postgres@localhost:5432/postgres"
    ACCESS_TOKEN_EXP: int = 86400
    JWT_SECRET_KEY: str = os.environ.get('JWT_SECRET_KEY')
    JWT_ALGORITHM: str = "HS256"

    class Config:
        case_sensitive = True


config = Settings(_env_file=get_env_file())
