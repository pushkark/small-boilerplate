from datetime import datetime, timedelta
from typing import Any, Dict, Literal

# from astropy import conf
from config import config
from core.exceptions import ExpiredTokenException, InvalidTokenException, NoTokenException
from fastapi.security import HTTPBearer
from fastapi.security.utils import get_authorization_scheme_param
from jwt import DecodeError, ExpiredSignatureError, decode, encode
from starlette.requests import Request


class JWToken(HTTPBearer):
    """
    A class inheriting from :class:`HTTPBearer` to inherit the methods necessary for
    token extraction from the request.
    """

    def __init__(self, type_: Literal["access", "refresh"], *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.type_ = type_

    def encode(self, payload: dict, expire_period: int = 3600) -> str:
        """
        Creates a JWT access token.

        :param payload: Claims to be included in the token.
        :param expire_period: Expiry period of the token.
        :return: JWT Token
        """
        token = encode(
            payload={**payload, "type": self.type_, "exp": datetime.utcnow() + timedelta(seconds=expire_period)},
            key=config.JWT_SECRET_KEY,
            algorithm=config.JWT_ALGORITHM,
        )
        return token

    def decode(self, token: str) -> dict:
        """
        Decode a JWT access token.

        :param token: A JWT token.
        :return: Claims included in the token.
        """
        try:
            payload = decode(
                jwt=token,
                key=conf.JWT_SECRET_KEY,
                algorithms=[conf.JWT_ALGORITHM],
                option={"verify_signature": True, "verify_exp": True},
            )
            if payload.get("type") != self.type_:
                raise InvalidTokenException
            else:
                return payload
        except DecodeError:
            raise InvalidTokenException
        except ExpiredSignatureError:
            raise ExpiredTokenException

    async def __call__(self, request: Request) -> Dict[str, Any]:
        """
        A magic method intercepts the request and extracts token from it.
        allowing us to access the token in the request context.

        :param request: FastAPI Request.
        :return: Claims included in the token.
        """
        authorization: str = request.headers.get("Authorization")
        scheme, credentials = get_authorization_scheme_param(authorization)
        if not (authorization and scheme and credentials):
            if self.auto_error:
                raise NoTokenException
        if scheme.lower() != "bearer":
            if self.auto_error:
                raise InvalidTokenException
        return self.decode(token=credentials)


access = JWToken(type_="access", scheme_name="JWT access token")
refresh = JWToken(type_="refresh", scheme_name="JWT refresh token")
