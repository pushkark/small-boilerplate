from pydantic import BaseModel
from pydantic.utils import to_lower_camel


class APIModel(BaseModel):
    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True
        use_enum_values = True