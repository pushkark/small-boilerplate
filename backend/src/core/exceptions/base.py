from starlette import status


class CustomException(Exception):
    """
    A custom exception class to raise neccessary exceptions in the app.
    """

    status_code = status.HTTP_502_BAD_GATEWAY
    message = status.HTTP_502_BAD_GATEWAY

    def __init__(self, message=None):
        if message:
            self.message = message


class BadRequestException(CustomException):
    """
    Returns :class:`JSONResponse` with desired status code.
    """

    status_code = status.HTTP_400_BAD_REQUEST
    message = status.HTTP_400_BAD_REQUEST


class NotFoundException(CustomException):
    """
    Returns :class:`JSONResponse` with desired status code.
    """

    status_code = status.HTTP_400_BAD_REQUEST
    message = status.HTTP_404_NOT_FOUND


class ForbiddenException(CustomException):
    """
    Returns :class:`JSONResponse` with desired status code.
    """

    status_code = status.HTTP_403_FORBIDDEN
    message = status.HTTP_403_FORBIDDEN


class UnauthorizedException(CustomException):
    """
    Returns :class:`JSONResponse` with desired status code.
    """

    status_code = status.HTTP_401_UNAUTHORIZED
    message = status.HTTP_401_UNAUTHORIZED


class UnprocessableEntityException(CustomException):
    """
    Returns :class:`JSONResponse` with desired status code.
    """

    status_code = status.HTTP_422_UNPROCESSABLE_ENTITY
    message = status.HTTP_422_UNPROCESSABLE_ENTITY


class AlreadyExistsException(CustomException):
    """
    Returns :class:`JSONResponse` with desired status code.
    """

    status_code = status.HTTP_409_CONFLICT
    message = status.HTTP_409_CONFLICT
