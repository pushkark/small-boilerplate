from core.exceptions.base import UnauthorizedException, UnprocessableEntityException


class NoTokenException(UnauthorizedException):
    """
    Returns :class:`JSONResponse` with desired status code.
    """

    message = "NO_TOKEN"


class InvalidTokenException(UnprocessableEntityException):
    """
    Returns :class:`JSONResponse` with desired status code.
    """

    message = "INVALID_TOKEN"


class ExpiredTokenException(UnauthorizedException):
    """
    Returns :class:`JSONResponse` with desired status code.
    """

    message = "TOKEN_EXPIRED"
