from sqlalchemy import create_engine

from sqlalchemy import create_engine, ForeignKey
from sqlalchemy.engine import URL
from sqlalchemy.ext.asyncio import async_session
from sqlalchemy.orm import declarative_base, sessionmaker, Mapped, mapped_column, DeclarativeBase

DATABASE_URL = URL.create(
    drivername="postgresql",
    username="postgres",
    password="postgres",
    host="localhost",
    database="postgres",
    port="5432"
)

Base = declarative_base()
engine = create_engine(DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


async def db_session() -> AsyncSession:
    """
    Database Session Generator.
    :return: A database session.
    """
    async with async_session() as session, session.begin():  # type: AsyncSession
        try:
            yield session
        except Exception:
            await session.rollback()
            await session.close()
            raise

class User(Base):
    __tablename__ = "user"

    id: Mapped[uuid.UUID] = mapped_column(default=uuid.uuid4, primary_key=True, index=True)
    user_first_name: Mapped[str] = mapped_column(index=True, nullable=False)
    user_last_name: Mapped[str] = mapped_column(index=True, nullable=False)
    mobile_no: Mapped[str] = mapped_column(index=True, nullable=False)
    email: Mapped[str]= mapped_column(unique=True, nullable=False)
    password:Mapped[str]=mapped_column(index=True, nullable=False)


class OTP (Base):
    __tablename__ = "otp"
    id: Mapped[uuid.UUID] = mapped_column(default=uuid.uuid4, primary_key=True, index=True)
    otp:Mapped[int] = mapped_column(index=True, nullable=False)
    email :Mapped[str]=mapped_column( ForeignKey('user.email'), nullable=False)


