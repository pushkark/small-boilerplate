from sqlalchemy import Column, DateTime, func
from sqlalchemy.ext.declarative import declared_attr


class TimestampMixin:
    """
    A Mixin class that can be user in SQLAlchemy db models where resective fields are
    needed.
    """

    @declared_attr
    def created_at(cls):
        """
        Defining a created at column.
        """
        return Column(DateTime, nullable=True)

    @declared_attr
    def updated_at(cls):
        """
        Defining a updated at column.
        """
        return Column(DateTime, onupdate=func.now(), nullable=True)
